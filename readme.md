## Example Laravel API

 This is a sample project to write an API using Laravel 6. 
 
 
You can use Homestead, a Vagrant box specially crafted for Laravel. 
Refer to the Homestead documentation.

 - https://laravel.com/docs/6.x/homestead

## Running the API
Create the database (and database user if necessary) and add them to the `.env` file.

```
DB_DATABASE=db_name
DB_USERNAME=db_user
DB_PASSWORD=password
```

Then composer install, migrate, seed, all that jazz:

1. `composer install`
2. `php artisan migrate`
3. `php artisan db:seed`
4. `php artisan serve` | (homestead running)

The API will be running on `localhost:8000`.

## Running tests
1. `composer test`

## Postman collection
1. https://documenter.getpostman.com/view/120405/SzKYNwBd?version=latest```

