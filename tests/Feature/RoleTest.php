<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Tests\TestCase;

/**
 * Class RoleTest
 * @package Tests\Feature
 */
class RoleTest extends TestCase
{
    public function testsRoleAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'Lorem',
            'level' => '2'
        ];

        $this->json('POST', '/api/role', $payload, $headers)
            ->assertJson(['name' => 'Lorem', 'level' => '2']);
    }

    public function testsRoleAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $role = factory(Role::class)->create([
            'name' => 'First Role',
            'level' => '2'
        ]);

        $payload = [
            'id' => "1",
            'name' => 'Update role name',
            'level' => '2'
        ];

        $response = $this->json('PUT', '/api/role/' . $role->id, $payload, $headers)
            ->assertJson(['name' => 'Update role name', 'level' => '2']);
    }

    public function testsRolesAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $role = factory(Role::class)->create([
            'name' => 'First Role',
        ]);

        $this->json('DELETE', '/api/role/' . $role->id, [], $headers)
            ->assertStatus(204);
    }

    public function testRoleAreListedCorrectly()
    {
        factory(Role::class)->create([
            'name' => 'First Role', 'level' => '1'
        ]);

        factory(Role::class)->create([
            'name' => 'Second Role', 'level' => '2'
        ]);

        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/role', [], $headers)
            ->assertStatus(200)
            ->assertJsonFragment(['name' => 'First Role', 'level' => '1'])
            ->assertJsonFragment(['name' => 'Second Role', 'level' => '2'])
            ->assertJsonStructure([
                '*' => ['name', 'level'],
            ]);
    }

    public function testUserCantAccessRolesWithWrongToken()
    {
        factory(Role::class)->create();
        $user = factory(User::class)->create(['email' => 'user@test.com']);
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $user->generateToken();

        $this->json('get', '/api/role', [], $headers)->assertStatus(401);
    }

    public function testUserCantAccessRolesWithoutToken()
    {
        factory(Role::class)->create();

        $this->json('get', '/api/role')->assertStatus(401);
    }
}
