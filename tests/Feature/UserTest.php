<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

/**
 * Class UserTest
 * @package Tests\Feature
 */
class UserTest extends TestCase
{
    public function testsUserAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'Johnny',
            'email' => 'Johnny@example.com',
            'password' => 'doe',
            'role_id' => '1',
        ];

        $this->json('POST', '/api/user', $payload, $headers)
            ->assertJson([
                'name' => 'Johnny',
                'email' => 'Johnny@example.com',
                'password' => 'doe',
                'role' => null,
            ]);
    }

    public function testsUserAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $user = factory(User::class)->create([
            'name' => 'John',
            'email' => 'email@example.com',
            'password' => 'doe',
            'role_id' => '1',
        ]);

        $payload = [
            'name' => 'John',
            'email' => 'aaaaaemail@example.com',
            'password' => 'doe',
            'role_id' => '1',
        ];

        $response = $this->json('PUT', '/api/user/' . $user->id, $payload, $headers)
            ->assertJson([
                'name' => 'John',
                'email' => 'email@example.com',
                'password' => 'doe',
                'phone' => NULL,
                'country' => 'Portugal',
                'api_token' => NULL,
            ]);
    }

    public function testsUserAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $user = factory(User::class)->create([
            'name' => 'John',
            'email' => 'email@example.com',
            'password' => 'doe',
            'role_id' => '1',
        ]);

        $this->json('DELETE', '/api/user/' . $user->id, [], $headers)
            ->assertStatus(204);
    }

    public function testUserCantAccessRolesWithoutToken()
    {
        factory(User::class)->create();

        $this->json('get', '/api/user')->assertStatus(401);
    }
}
