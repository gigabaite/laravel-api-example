<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Validator;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        return User::where("user_id", "=", $request->user()->id)->get();
    }

    /**
     * @param User $user
     * @return User
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'role_id' => 'required|exists:roles,id',
            'user_id' => 'exists:users,id'
        ];
        $messages = [
            'email.unique' => 'invalid :attribute. already exists',
            'role_id' => ':attribute invalid role',
            'user_id' => 'invalid :attribute'
        ];
        $this->validate($request, $rules, $messages);

        $owner = Auth::user();

        $user = User::create($request->toArray());
        $user->user_id = (int)$request->user()->id;
        $owner->users()->save($user);

        return response()->json($user->load('role')->toArray());
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, User $user)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
            'password' => 'required',
            'role_id' => 'required|exists:roles,id',
            'user_id' => 'exists:users,id'
        ];
        $messages = [
            'email.unique' => 'invalid :attribute. already exists',
            'role_id' => ':attribute invalid role',
            'user_id' => 'invalid :attribute'
        ];
        $this->validate($request, $rules, $messages);

        $owner = Auth::user();
        $user->user_id = (int)$request->user()->id;
        $owner->users()->save($user);

        return response()->json($user);
    }

    /**
     * @param User $user
     * @return JsonResponse
     */
    public function delete(User $user)
    {
        try {
            $owner = Auth::user();
            $owner->where('id', '=', $user->id)
                ->delete();
        } catch (Exception $e) {
        }

        return response()->json("ok", 204);
    }
}
