<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class RoleController
 * @package App\Http\Controllers
 */
class RoleController extends Controller
{
    /**
     * @return Role[]|Collection
     */
    public function index()
    {
        return Role::all();
    }

    /**
     * @param Role $role
     * @return Role
     */
    public function show(Role $role)
    {
        return $role;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'level' => 'required|exists:roles,level'
        ];
        $messages = [
            'level' => 'invalid :attribute'
        ];
        $this->validate($request, $rules, $messages);

        $role = Role::create($request->toArray());

        return response()->json($role);
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return JsonResponse
     */
    public function update(Request $request, Role $role)
    {
        $rules = [
            'name' => 'required',
            'level' => 'required|exists:roles,level'
        ];
        $messages = [
            'level' => 'invalid :attribute'
        ];
        $this->validate($request, $rules, $messages);

        $role->update($request->all());

        return response()->json($role);
    }

    /**
     * @param Role $role
     * @return JsonResponse
     */
    public function delete(Role $role)
    {
        try {
            $role->delete();
        } catch (Exception $e) {
        }

        return response()->json(null, 204);
    }
}
